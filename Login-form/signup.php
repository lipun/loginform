<!DOCTYPE html>
<html>
<head>
	<title>SIGN UP</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
     <form action="" method="post">
     	<h2>SIGN UP</h2>
          <?php if (isset($_GET['error'])) { ?>
     		<p class="error"><?php echo $_GET['error']; ?></p>
     	<?php } ?>

          <?php if (isset($_GET['success'])) { ?>
               <p class="success"><?php echo $_GET['success']; ?></p>
          <?php } ?>
          

          <label>User Name</label>
          <input type="text" name="uname" placeholder="User Name" id="uname">
          <span id="unameError" style="color:red;display:none;margin-left:10px;">User name must not be empty</span><br>
          
     	<label>Password</label>
     	<input type="password" name="password" placeholder="Password" id="password">
          <span id="passwordError" style="color:red;display:none;margin-left:10px;">Password name must not be empty</span><br>

          <label>Re Password</label>
          <input type="password" name="re_password" placeholder="Re_Password" id="re_password">
          <span id="re_passwordError" style="color:red;display:none;margin-left:10px;"></span><br>

     	<button type="submit" id="submit">Sign Up</button>
          <a href="index.php" class="ca">Already have an account?</a>
     
     </form>

     <script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#submit').click(function(){ 
					var uname=$('#uname');
					var password=$('#password');
					var re_password=$('#re_password');

					if(uname.val()==''){
						$('#unameError').show();
						uname.focus();
						return false;
					} else {
						$('#unameError').hide();
					}

					if ( uname.val().match('^[a-zA-Z]{3,16}$') ) {

					} else {
						$('#unameError').text('invalid name');
						$('#unameError').show();
						uname.focus();
						return false;
					}
					if(password.val()==''){
						$('#passwordError').show();
						password.focus();
						return false;

					} 

					if(password.val().length<8)
					{
						$('#passwordError').text('password must greater than 8 characters');
						$('#passwordError').show();
						return false;
					} else {
						$('#passwordError').hide();
					}


					console.log(password.val());
					console.log(re_password.val());
					if(password.val() != re_password.val()) {
						$('#re_passwordError').text('password 1 and 2 should be same');
						$('#re_passwordError').show();
						return false;
					}
					else{
						$('#re_passwordError').hide();
					}
					
				});

			});

               function ispassword(password){ 
				var p= /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])$/;
				if (p.test(password)) {
					return true;
				}
				else{
					return false;
				}
			}



		</script>

<?php 
session_start(); 
include "db_conn.php";

if (isset($_POST['uname']) && isset($_POST['password'])) {

	$uname = $_POST['uname'];
	$re_password = $_POST['re_password'];
     $user_data = 'uname=';

        
		$sql2 = "INSERT INTO users(user_name, password) VALUES('$uname', '$re_password')";
		$result2 = mysqli_query($conn, $sql2);
		if ($result2) {
               header("Location: signup.php?success=Your account has been created successfully");
           exit();
         }else {
                   header("Location: signup.php?error=unknown error occurred&$user_data");
               exit();
         }
	
}
 ?>


</body>
</html>